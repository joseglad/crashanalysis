package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import controllers.InitBuckets;
import controllers.StacktraceCleaner;
import models.Bucket;
import models.Stacktrace;

public class RandomMain {
	public static final String TESTING = "data/nautilus/nautilus-testing/";
	public static final int MIN = 0;

	public static void main(String[] args) throws FileNotFoundException {
		System.out.println("Main has launched");
		System.out.println("Buckets initializing");
		ArrayList<Bucket> buckets = InitBuckets.init();
		System.out.println("Buckets initialized");
		ArrayList<Stacktrace> stacktraces = new ArrayList<Stacktrace>();
		String[] bucketNames = new String[buckets.size()];
		String line = "";
		Random rn = new Random();
		//int answer = rn.nextInt(10) + 1;
		int MAX = buckets.size() - 1;
		
		for(int i = 0; i < buckets.size(); i++) {
			bucketNames[i] = buckets.get(i).getName();
		}
		
		File file = new File(TESTING);
		final File[] children = file.listFiles(new FileFilter() {
	    	public boolean accept(File f) {
	                return f.getName().endsWith(".txt") ;
	        }}
	    );
		
		
		Integer[] index = new Integer[children.length];
		HashMap<Integer, String> results = new HashMap<Integer, String>();
		
		for(int i = 0; i < children.length; i++) {
			Stacktrace trace = new Stacktrace();
			trace.setName(children[i].getName().replaceAll(".txt", ""));

			stacktraces.add(trace);
			Integer integer = Integer.decode(trace.getName());
			index[i] = integer;
			results.put(integer, stacktraces.get(i).getName() + "  -> " + bucketNames[rn.nextInt(MAX) + MIN]);
		}
		
		// Tri des index
				for(int i = 0; i < index.length; i++) {
					for(int j = 0; j < index.length; j++) {
						if(index[i] < index[j]) {
							int aux = index[i];
							index[i] = index[j];
							index[j] = aux;
						}
					}
				}
				
				for(int i = 0; i < index.length; i++) {
					System.out.println(results.get(index[i]));
				}
		

	}

}
