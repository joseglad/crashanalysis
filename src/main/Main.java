package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import models.Bucket;
import models.Stacktrace;
import controllers.InitBuckets;
import controllers.StacktraceCleaner;

public class Main {
	public static final String TESTING = "data/nautilus/nautilus-testing/";
	//public static final String TESTING = "data/dataset2/testing/";
	public static final int TOLERANCE = 3;
	public static final int BL = 0;
	private static final int MAX_LINE = 1000; //1700 semble intéressant et 10000 c'est trop

	public static void main(String[] args) throws FileNotFoundException {
		String line = "";
		
		ArrayList<Bucket> buckets = InitBuckets.init();
		//Bucket bucket = buckets.get(0);
		
		
		ArrayList<Stacktrace> stacktraces = new ArrayList<Stacktrace>();
		
		
		System.out.println("Main has launched");		
		
		File file = new File(TESTING);
		final File[] children = file.listFiles(new FileFilter() {
	    	public boolean accept(File f) {
	                return f.getName().endsWith(".txt") ;
	        }}
	    );
		

		
		for(int i = 0; i < children.length; i++) {
			@SuppressWarnings("resource")
			BufferedReader reader = new BufferedReader(new FileReader(children[i]));
			Stacktrace trace = new Stacktrace();
			trace.setName(children[i].getName().replaceAll(".txt", ""));
			boolean fileIsFurther = false;
			
			try {
				while((line = reader.readLine()) != null) {
					if(line.startsWith("#")) {
						trace.setNumberOfLines(trace.getNumberOfLines() + 1);
						String cleanLine = StacktraceCleaner.clean(line);
						trace.addToContent(cleanLine);
						if(!line.contains(".c")) {
							fileIsFurther = true;
						}
					} else {
						if(fileIsFurther) {
							trace.setNumberOfLines(trace.getNumberOfLines() + 1);
							String cleanLine = StacktraceCleaner.clean(line);
							trace.addToContent(cleanLine);
							if(line.contains(".c")) {
								fileIsFurther = false;
							}
						}
					}
					//String cleanLine = StacktraceCleaner.clean(line);
					//trace.addToContent(cleanLine);
				}
				
			} catch(IOException e) {
				System.out.println("Erreur de lecture du ficher ");
			}
			
			stacktraces.add(trace);			
		}
		
		System.out.println("Buckets calcul des matches pour chaque bucket.");
		
		for(int b = 0; b < buckets.size(); b++) {
			Bucket bucket = buckets.get(b);
			//String match = InitBuckets.bucketMatch(bucket);
			//bucket.setMatch(match);

			bucket.initBucketCommonWords();
		}
		
		
		System.out.println("Fin Buckets calcul des matches pour chaque bucket.");
		
		System.out.println("Debut du remplissage des stacktraces.");
		System.out.println("Nombre de stacktraces : " + stacktraces.size());
		for(int s = 0; s < stacktraces.size(); s++) {
			//System.out.print(s + ". ");
			Stacktrace trace = stacktraces.get(s);
			System.out.println("Index : "+ s + ", Nom : " + trace.getName() + " ");
			
			//System.out.println(trace.getName());
			int bucketSize = buckets.size();
			trace.initScoreArray(bucketSize);
			if(trace.getNumberOfLines() > MAX_LINE) {
				continue;
			}
			
			for(int b = 0; b < bucketSize; b++) {
				//String match = StacktraceMatcher.match(buckets.get(b).getMatch(), trace.getContent());
				String match = InitBuckets.bucketMatch(buckets.get(b), trace.getContent());
				trace.addScore(b, match.length());
				
				//ICI compter les mots en commun et faire trace.addScore(b, nombre de mot en commun);
				//trace.addScore(b, buckets.get(b).getNumberOfCommonWords(trace.getContent()));
				
				/*
				int distance = trace.getNumberOfLines() - buckets.get(b).getAverageOfNumberOfLines();
				if(distance < 0) {
					distance *= -1;
				}
				
				if(0 <= distance && distance <= TOLERANCE) {
					trace.addScore(b, buckets.get(b).getAverageOfNumberOfLines());
				}
				*/
			}
		}
		System.out.println();
		System.out.println("Fin du remplissage des stacktraces.");
		
		Integer[] index = new Integer[stacktraces.size()];
		HashMap<Integer, String> results = new HashMap<Integer, String>();
		
		for(int s = 0; s < stacktraces.size(); s++) {
			Stacktrace trace = stacktraces.get(s);
			Integer integer = Integer.decode(trace.getName());
			index[s] = integer;
			results.put(integer, trace.getName() + "  -> " + buckets.get(trace.getBucketIndex()).getName());
		}
		
		// Tri des index
		for(int i = 0; i < index.length; i++) {
			for(int j = 0; j < index.length; j++) {
				if(index[i] < index[j]) {
					int aux = index[i];
					index[i] = index[j];
					index[j] = aux;
				}
			}
		}
		
		for(int i = 0; i < index.length; i++) {
			System.out.println(results.get(index[i]));
		}
		
		
		System.out.println("\nMain has finished");
		//System.out.println(match);
	}

}
