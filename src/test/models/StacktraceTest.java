package test.models;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Stacktrace;

public class StacktraceTest {

	@Test
	public void testGetBucketIndex() {
		Stacktrace trace = new Stacktrace();
		trace.initScoreArray(5);
		trace.addScore(2, 10);
		
		assertEquals(2, trace.getBucketIndex());
	}

}
