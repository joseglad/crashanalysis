package test.controllers;

import static org.junit.Assert.*;

import org.junit.Test;

import controllers.StacktraceCleaner;
import controllers.StacktraceMatcher;

public class StacktraceMatcherTest {

	@Test
	public void testMatch() {
		String match = "je suis";
		String trace1 = "je suis là";
		String trace2 = "je suis ailleurs";
		System.out.println(StacktraceMatcher.match(trace1, trace2));
		assertEquals(match, StacktraceMatcher.match(trace1, trace2));
	}

}
