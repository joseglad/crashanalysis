package test.controllers;

import static org.junit.Assert.*;

import org.junit.Test;

import controllers.StacktraceCleaner;

public class StacktraceCleanerTest {
	
	@Test
	public void testClean() {
		String correctLine = "NUMBER ADDRESS in g_hash_table_foreach_remove_or_steal (hash_table=ADDRESS,";
		String stacktraceLine = "#0  0xb732c2eb in g_hash_table_foreach_remove_or_steal (hash_table=0x80fed80, ";
		
		assertEquals(correctLine, StacktraceCleaner.clean(stacktraceLine));
	}

}
