package controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import models.Bucket;
import models.Stacktrace;

public class InitBuckets {
	public static final String TRAINING = "data/nautilus/nautilus-training";
	//public static final String TRAINING = "data/dataset2/training";
	
	public static ArrayList<Bucket> init() {
		File training = new File(InitBuckets.TRAINING);
		String line = "";
		
		final File[] buckets = training.listFiles(new FileFilter() {
	    	public boolean accept(File f) {
	                return true ;
	        }}
	    );
		
		//System.out.println(buckets[0].getName());
		ArrayList<Bucket> bucketArray = new ArrayList<Bucket>();
		for(int i = 0; i < buckets.length; i++) {
			Bucket b = new Bucket();
			b.setName(buckets[i].getName());
			
			final File[] traces = buckets[i].listFiles(new FileFilter() {
		    	public boolean accept(File f) {
		                return true ;
		        }}
		    );
			
			for(int t = 0; t < traces.length; t++) {
				final File[] contents = traces[t].listFiles(new FileFilter() {
			    	public boolean accept(File f) {
			                return true ;
			        }}
			    );
				
				for(int c = 0; c < contents.length; c++) {
					BufferedReader reader;
					Stacktrace trace = new Stacktrace();
					boolean fileIsFurther = false;
					
					try {
						reader = new BufferedReader(new FileReader(contents[c]));
						while((line = reader.readLine()) != null) {
							if(line.startsWith("#")) {
								trace.setNumberOfLines(trace.getNumberOfLines() + 1);
								String cleanLine = StacktraceCleaner.clean(line);
								trace.addToContent(cleanLine);
								if(!line.contains(".c")) {
									fileIsFurther = true;
								}
							} else {
								if(fileIsFurther) {
									trace.setNumberOfLines(trace.getNumberOfLines() + 1);
									String cleanLine = StacktraceCleaner.clean(line);
									trace.addToContent(cleanLine);
									if(line.contains(".c")) {
										fileIsFurther = false;
									}
								}
							}
						}
						
					} catch(IOException e) {
						System.out.println("Erreur de lecture du ficher " + contents[i].getName());
					}
					
					b.addStacktrace(trace);
				}
			}
			
			bucketArray.add(b);
		}
		return bucketArray;
	}
	
	public static void analyse(ArrayList<Bucket> buckets, ArrayList<Stacktrace> stacktraces) {
		// Lancer initBuckets
		
		// Ecrire la fonction qui calcule les matches d'un bucket et retourne le "match/s�quence" qui ont en commun
		// Remplir le scoreArray de chaque stacktrace avec les scores de match
		
	}
	
	/**
	 * Retourne le match commun entre les stacktraces du bucket
	 * S'il y a qu'un stacktrace, on retourne tout le contenu du stacktrace
	 * @param bucket � matcher
	 * @return le match commun
	 */
	public static String bucketMatch(Bucket bucket) {
		String match = "";
		ArrayList<Stacktrace> stacktraces = bucket.getStacktraces();
		int length = stacktraces.size();
		
		if(length == 1) {
			return stacktraces.get(0).getContent();
		} else if (length <= 0) {
			return "@NO_CONTENT@";
		} else {
			match = stacktraces.get(0).getContent();
			for(int i = 1; i < length; i++) {
				match = StacktraceMatcher.match(match, stacktraces.get(i).getContent());
			}
		}
		
		return match;
	}
	
	/**
	 * Retourne le match commun entre les stacktraces du bucket
	 * S'il y a qu'un stacktrace, on retourne tout le contenu du stacktrace
	 * @param bucket � matcher
	 * @return le match commun
	 */
	public static String bucketMatch(Bucket bucket, String content) {
		ArrayList<Stacktrace> stacktraces = bucket.getStacktraces();
		int length = stacktraces.size();
		String[] matches = new String[length];
		int index = 0;
		int matchLength = 0;
		
		if (length <= 0) {
			return "";
		} else {
			for(int i = 0; i < length; i++) {
				matches[i] = StacktraceMatcher.match(content, stacktraces.get(i).getContent());
				int l = matches[i].length();
				if(l > matchLength) {
					index = i;
					matchLength = l;
				}
			}			
		}
		
		return matches[index];
	}
}
