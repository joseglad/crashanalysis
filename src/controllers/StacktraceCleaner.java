package controllers;

public class StacktraceCleaner {
	
	public static String clean(String content) {
		String result = content;
		//result = result.replaceAll("#[0-9]*  0x[0-9a-zA-Z]+ in ?? ()", "VOID"); // me fait gagner 1 point
		
		//result = result.replaceAll("0x8[0-9a-zA-Z]+", "0x8_ADDRESS"); // ne sont pas pertinents
		//result = result.replaceAll("0xb7[0-9a-zA-Z]+", "0xb7_ADDRESS"); ne sont pas pertinents
		//result = result.replaceAll("([a-zA-Z]+=0x[0-9a-zA-Z]+)", "()"); // test� le 01/11/2016 => pas grand chose
		result = result.replaceAll("0x[0-9a-zA-Z]+", "ADDRESS");
		result = result.replaceAll("__val = \\{[0-9a-zA-Z, ]*\\}", " __val = {}");
		result = result.replaceAll("= ([0-9a-zA-Z*_]+)", "= VALUE"); //retirer ce qu'il y a dans les parenth�ses
		result = result.replace("\\", "");
		
		result = result.replaceAll("= [0-9a-zA-Z<>']+ ", "= VALUE");
		
		//result = result.replaceAll("/build/buildd/glib2.0-2.[0-9][0-9].[0-9]/glib/gthread.c:[0-9]+", "/build/buildd/glib2.0-2.X.X/glib/gthread.c:NUMBER"); //num�ro de ligne diff�rence selon la version de la biblioth�que
		result = result.replaceAll("#[0-9]*", "NUMBER");
		result = result.replaceAll("[0-9][0-9]+", "CHIFFRE");
		//result = result.replace("()|at|in|??|No|=", ""); //retire les petits caract�res, pas pertinents
		//result.replaceAll("IA__", "");
		//result = result.replaceAll(" ", ""); //les espaces sont importants
		//result = result.replaceAll("  ", ""); //les double espaces 01/11/2016 ne sont pas pertinents
		result = result.replaceAll("  ", " "); //
		return result.trim();
	}

}
