package controllers;

import java.util.ArrayList;

import models.Stacktrace;

public class StacktraceMatcher {
	
	/**
	 * Retourne une liste contenant les éléments identiques
	 */
	/*
	public static ArrayList<String> match(Stacktrace st1, Stacktrace st2) {
		ArrayList<String> sameWords = new ArrayList<String>();
		ArrayList<String> words1 = st1.getWords();
		ArrayList<String> words2 = st2.getWords();
		
		for(int i = 0; i < words1.size(); i++) {
			for(int j = 0; j < words2.size(); j++) {
				if(!sameWords.contains(words1.get(i)) && words1.get(i).equals(words2.get(j))) {
					sameWords.add(words1.get(i));
				}
			}
		}
		
		return sameWords;
	}
	*/
	
	
	/**
	 * Retourne le plus long string en commun
	 * @param content1 contenu d'un Stacktrace
	 * @param content2 contenu d'un Stacktrace
	 * @return le plus long string en commun
	 */
	public static String match(String content1, String content2) {
		String commonString = "";
		String[] array1 = content1.trim().split(" ");
		String[] array2 = content2.trim().split(" ");
		ArrayList<String> applicants = new ArrayList<String>();

		for(int i = 0; i < array1.length; i++) {
			int j = 0;
			int currentI = i;
			commonString = "";
			
			if(content2.contains(array1[i])) {
				while((j < array2.length) && (array1[i].equals(array2[j]) == false)) j++;
				
				if(j >= array2.length) continue;
				
				while(i < array1.length && j < array2.length && array1[i].equals(array2[j])) {
					commonString += " " + array1[i];
					i++;
					j++;
				}
			}
			applicants.add(commonString.trim());
			i = currentI;
		}
		
		for(int i = 0; i < applicants.size(); i++) {
			if(applicants.get(i).length() > commonString.length()) {
				commonString = applicants.get(i);
			}
		}
		
		return commonString;
	}
	
	/**
	 * Compare deux listes
	 * @param list1
	 * @param list2
	 * @return true ils sont identiques sinon false
	 */
	public static float compare(ArrayList<String> list1, ArrayList<String> list2) {
		float counter = (float) 0.0;
		
		for(int i = 0; i < list1.size(); i++) {
			for(int j = 0; j < list2.size(); j++) {
				if(list1.get(i).equals(list2.get(j))) {
					counter++;
					continue;
				}
			}
		}
		
		return counter / list1.size();		
	}
}
