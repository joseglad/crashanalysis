package models;

import java.util.ArrayList;

public class Bucket {
	private String name = null;
	private ArrayList<Stacktrace> traces = null;
	private String match = null;
	private ArrayList<String> commonWords = new ArrayList<String>();
	
	public Bucket() {
		this.traces = new ArrayList<Stacktrace>();
	}
	
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setMatch(String match) {
		this.match = match;
	}
	
	public String getMatch() {
		return this.match;
	}
	
	public void addStacktrace(Stacktrace trace) {
		this.traces.add(trace);
	}
	
	public ArrayList<Stacktrace> getStacktraces() {
		return this.traces;
	}
	
	public void initBucketCommonWords() {
		int size = this.traces.size();
		if(size >= 1) {
			String[] words = this.traces.get(0).getContent().split(" ");
			for(int w = 0; w < words.length; w++) {
				boolean everywhere = true;
				for(int i = 1; i < this.traces.size(); i++) {
					if(!this.traces.get(i).getContent().contains(words[w])) {
						everywhere = false;
						break;
					}					
				}
				
				if(everywhere && !this.commonWords.contains(words[w])) {
					this.commonWords.add(words[w]);
				}
			}
		} 		
	}
	
	public ArrayList<String> getCommonWords() {
		return this.commonWords;
	}
	
	public int getNumberOfCommonWords(String traceContent) {
		int counter = 0;
		//String[] bucketWords = this.
		for(int  i = 0; i < this.commonWords.size(); i++) {
			if(traceContent.contains(this.commonWords.get(i))) {
				counter++;
			}
		}
		return counter;
	}
	
	public int getAverageOfNumberOfWords() {
		int counter = 0;
		int length = this.traces.size();
		if(length == 0) return 0;
		for(int i = 0; i < length; i++) {
			counter += this.traces.get(i).getContent().split(" ").length;
		}
		
		return counter / length;
	}
	
	public int getAverageOfNumberOfLines() {
		int counter = 0;
		int length = this.traces.size();
		if(length == 0) return 0;
		for(int i = 0; i < length; i++) {
			counter += this.traces.get(i).getNumberOfLines();
		}
		
		return counter / length;		
	}
	
}
