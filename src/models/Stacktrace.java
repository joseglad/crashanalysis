package models;


public class Stacktrace {
	private int numberOfLines = 0; // nombre de ligne avec un numéro #, exemple #1, #2, ..
	//private ArrayList<String> words;
	private String content = "";
	private int[] scoreArray = null;
	private String name = null;
	private String[] words = null;
 	
	public Stacktrace() {
		//this.words = new ArrayList<String>();
	}
	
	/*
	public Stacktrace(ArrayList<String> words) {
		this.words = words;
	}
	*/
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	/*
	public void addWord(String word){
		this.words.add(word);
	}
	
	public ArrayList<String> getWords() {
		return this.words;
	}
	*/
	
	
	public void setNumberOfLines(int n) {
		this.numberOfLines = n;
	}
	
	public int getNumberOfLines() {
		return this.numberOfLines;
	}
	
	
	//A utiliser pour le nouveau algo
	
	public void addToContent(String line) {
		this.content += " " + line.trim();
	}
	
	public String getContent() {
		return this.content;
	}
	
	public void initScoreArray(int length) {
		this.scoreArray = new int[length];
		for(int i = 0; i < length; i++) {
			this.scoreArray[i] = 0;
		}
	}
	
	public void initWords() {
		this.words = this.content.split(" ");
	}
	
	public void addScore(int index, int score) {
		this.scoreArray[index] += score;
	}
	
	public int[] getScoreArray() {
		return this.scoreArray;
	}
	
	public int getBucketIndex() {
		int index = 0;
		int maxLength = 0;
		
		for(int i = 0; i < this.scoreArray.length; i++) {
			if(this.scoreArray[i] > maxLength) {
				index = i;
				maxLength = this.scoreArray[i];
			}
		}
		
		return index;
	}
}
