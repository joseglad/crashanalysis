Comme pour Twitter avec les N-grammes, en suivant la structure du code on peut faire ressortir les ressemblances.

1) Nettoyer/remplacer le Stacktrace des éléments qui nous paraissent inutiles pour l'analyse (pour le moment).
2) Retourner une chaîne de caractères representant tout le contenu du Stacktrace.
3) Ecrire une méthode qui analyse et trouve la plus grande partie commune entre deux Stacktrace afin de stocker cette information dans le Bucket.
4) Ecrire une méthode qui permet de dire que c'est le Bucket qui a la plus grande partie commune qui l'emporte sur le choix final de l'appartenance du Stacktrace.
